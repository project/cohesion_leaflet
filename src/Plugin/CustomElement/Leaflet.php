<?php

namespace Drupal\cohesion_leaflet\Plugin\CustomElement;

use Drupal\cohesion_elements\CustomElementPluginBase;

/**
 * Cohesion Leaflet map.
 *
 * @package Drupal\cohesion\Plugin\CustomElement
 *
 * @CustomElement(
 *   id = "cohesion_leaflet",
 *   label = @Translation("Cohesion Leaflet map")
 * )
 */
class Leaflet extends CustomElementPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    return [
      'latitude' => [
        // This is the bootstrap class name that will be applied to the wrapping column.
        'htmlClass' => 'col-xs-12',
        // All form elements require a title.
        'title' => 'Latitude',
        // The field type.
        'type' => 'textfield',
        // These fields are specific to this form field type.
        'placeholder' => '0.0000',
        'required' => TRUE,
        'validationMessage' => 'This field is required.',
      ],
      'longitude' => [
        // This is the bootstrap class name that will be applied to the wrapping column.
        'htmlClass' => 'col-xs-12',
        // All form elements require a title.
        'title' => 'Longitude',
        // The field type.
        'type' => 'textfield',
        // These fields are specific to this form field type.
        'placeholder' => '0.0000',
        'required' => TRUE,
        'validationMessage' => 'This field is required.',
      ],
      'zoom' => [
        // This is the bootstrap class name that will be applied to the wrapping column.
        'htmlClass' => 'col-xs-12',
        // All form elements require a title.
        'title' => 'Map Zoom',
        // The field type.
        'type' => 'textfield',
        // These fields are specific to this form field type.
        'placeholder' => '13',
        'required' => TRUE,
        'validationMessage' => 'This field is required.',
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($element_settings, $element_markup, $element_class, $element_context = []) {
    // Render the element.
    return [
      '#theme' => 'cohesion_leaflet',
      '#template' => 'cohesion-leaflet-template',
      '#elementSettings' => $element_settings,
      '#elementMarkup' => $element_markup,
      '#elementContext' => $element_context,
      '#elementClass' => $element_class,
    ];
  }

}
